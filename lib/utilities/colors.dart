import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xFFE41C38);
  static const Color textPrimary = Colors.black;
  static const Color textSecondary = Color(0xFF303335);
  static const Color transparent = Colors.transparent;
}
