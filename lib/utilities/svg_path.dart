class AssetSVG {
  static const String home = 'assets/svg/Home.svg';
  static const String homeFill = 'assets/svg/Home_fill.svg';
  static const String heart = 'assets/svg/Heart.svg';
  static const String heartFill = 'assets/svg/Heart_fill.svg';
  static const String discovery = 'assets/svg/Discovery.svg';
  static const String discoveryFill = 'assets/svg/Discovery_fill.svg';
  static const String ticket = 'assets/svg/Ticket.svg';
  static const String ticketFill = 'assets/svg/Ticket_fill.svg';
  static const String profile = 'assets/svg/Profile.svg';
  static const String profileFill = 'assets/svg/Profile_fill.svg';
  static const String location = 'assets/svg/Location.svg';
  static const String notification = 'assets/svg/Notification.svg';
  static const String scan = 'assets/svg/Scan.svg';
  static const String parallelogram =
      'assets/svg/noun-parallelogram-cropped.svg';
}
