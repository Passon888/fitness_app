import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class IconWithText extends StatelessWidget {
  final SvgPicture svg;
  final String text;
  const IconWithText({super.key, required this.svg, required this.text});

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        svg,
        Text(
          text,
          style: const TextStyle(
              fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
        )
      ],
    );
  }
}
