import 'package:fitness_app/utilities/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../screens/exercise.dart';
import '../screens/home.dart';
import '../screens/myclass.dart';
import '../screens/privilege.dart';
import '../screens/profile.dart';
import '../utilities/svg_path.dart';

class TabletBody extends StatefulWidget {
  const TabletBody({super.key});

  @override
  State<TabletBody> createState() => _TabletBodyState();
}

class _TabletBodyState extends State<TabletBody> {
  int _currentIndex = 0;
  bool isExpanded = true;
  final screen = [
    const HomeScreen(),
    const MyClassScreen(),
    const ExerciseScreen(),
    const PrivilegeScreen(),
    const ProfileScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          NavigationRail(
            selectedIndex: _currentIndex,
            extended: isExpanded,
            onDestinationSelected: (int index) {
              setState(() {
                _currentIndex = index;
              });
            },
            backgroundColor: AppColors.primaryColor,
            unselectedIconTheme:
                const IconThemeData(color: Colors.white, opacity: 1),
            unselectedLabelTextStyle: const TextStyle(
              color: Colors.white,
            ),
            selectedIconTheme: const IconThemeData(
              color: AppColors.primaryColor,
            ),
            selectedLabelTextStyle: const TextStyle(
              color: Colors.white,
            ),
            destinations: [
              NavigationRailDestination(
                icon: SvgPicture.asset(
                  AssetSVG.home,
                ),
                selectedIcon: SvgPicture.asset(
                  AssetSVG.homeFill,
                  colorFilter: const ColorFilter.mode(
                      AppColors.primaryColor, BlendMode.srcIn),
                ),
                label: const Text("หน้าหลัก"),
              ),
              NavigationRailDestination(
                icon: SvgPicture.asset(
                  AssetSVG.heart,
                ),
                selectedIcon: SvgPicture.asset(
                  AssetSVG.heartFill,
                  colorFilter: const ColorFilter.mode(
                      AppColors.primaryColor, BlendMode.srcIn),
                ),
                label: const Text("คลาสของฉัน"),
              ),
              NavigationRailDestination(
                icon: SvgPicture.asset(
                  AssetSVG.discovery,
                ),
                selectedIcon: SvgPicture.asset(
                  AssetSVG.discoveryFill,
                  colorFilter: const ColorFilter.mode(
                      AppColors.primaryColor, BlendMode.srcIn),
                ),
                label: const Text("ออกกำลัง"),
              ),
              NavigationRailDestination(
                icon: SvgPicture.asset(
                  AssetSVG.ticket,
                ),
                selectedIcon: SvgPicture.asset(
                  AssetSVG.ticketFill,
                  colorFilter: const ColorFilter.mode(
                      AppColors.primaryColor, BlendMode.srcIn),
                ),
                label: const Text("สิทธิพิเศษ"),
              ),
              NavigationRailDestination(
                icon: SvgPicture.asset(
                  AssetSVG.profile,
                ),
                selectedIcon: SvgPicture.asset(
                  AssetSVG.profileFill,
                  colorFilter: const ColorFilter.mode(
                      AppColors.primaryColor, BlendMode.srcIn),
                ),
                label: const Text("โปรไฟล์"),
              ),
            ],
            trailing: Padding(
              padding: const EdgeInsets.only(top: 50),
              child: IconButton(
                onPressed: () {
                  setState(() {
                    isExpanded = !isExpanded;
                  });
                },
                icon: Icon(isExpanded
                    ? Icons.arrow_back_ios
                    : Icons.arrow_forward_ios),
              ),
            ),
          ),
          Expanded(
            child: Scaffold(
              body: screen[_currentIndex],
            ),
          )
        ],
      ),
    );
  }
}
