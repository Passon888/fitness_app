import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../screens/exercise.dart';
import '../screens/home.dart';
import '../screens/myclass.dart';
import '../screens/privilege.dart';
import '../screens/profile.dart';
import '../utilities/colors.dart';
import '../utilities/svg_path.dart';

class MobileBody extends StatefulWidget {
  const MobileBody({super.key});

  @override
  State<MobileBody> createState() => _MobileBodyState();
}

class _MobileBodyState extends State<MobileBody> {
  int currentIndex = 0;
  final screen = [
    const HomeScreen(),
    const MyClassScreen(),
    const ExerciseScreen(),
    const PrivilegeScreen(),
    const ProfileScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screen[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        selectedFontSize: 12,
        currentIndex: currentIndex,
        onTap: (index) => setState(() => currentIndex = index),
        unselectedItemColor: Colors.black,
        unselectedLabelStyle: const TextStyle(color: Colors.black),
        selectedItemColor: AppColors.primaryColor,
        showUnselectedLabels: true,
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AssetSVG.home,
            ),
            activeIcon: SvgPicture.asset(
              AssetSVG.homeFill,
              colorFilter: const ColorFilter.mode(
                  AppColors.primaryColor, BlendMode.srcIn),
            ),
            label: 'หน้าหลัก',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AssetSVG.heart,
            ),
            activeIcon: SvgPicture.asset(
              AssetSVG.heartFill,
              colorFilter: const ColorFilter.mode(
                  AppColors.primaryColor, BlendMode.srcIn),
            ),
            label: 'คลาสของฉัน',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AssetSVG.discovery,
            ),
            activeIcon: SvgPicture.asset(
              AssetSVG.discoveryFill,
              colorFilter: const ColorFilter.mode(
                  AppColors.primaryColor, BlendMode.srcIn),
            ),
            label: 'ออกกำลัง',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AssetSVG.ticket,
            ),
            activeIcon: SvgPicture.asset(
              AssetSVG.ticketFill,
              colorFilter: const ColorFilter.mode(
                  AppColors.primaryColor, BlendMode.srcIn),
            ),
            label: 'สิทธิพิเศษ',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AssetSVG.profile,
            ),
            activeIcon: SvgPicture.asset(
              AssetSVG.profileFill,
              colorFilter: const ColorFilter.mode(
                  AppColors.primaryColor, BlendMode.srcIn),
            ),
            label: 'โปรไฟล์',
          ),
        ],
      ),
    );
  }
}
