import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../components/promotion_card.dart';
import '../models/news.dart';
import '../models/promotion.dart';
import '../utilities/colors.dart';
import '../utilities/svg_path.dart';
import '../widgets/iconwithtext.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String homeTitle = 'ณัฐวัฒน์';
  String locationTitle = 'เกตเวย์ (บางซื่อ)';

  //List of Object Promotion
  List<Promotion> promotionData = [
    Promotion(
        imagePath:
            'https://www.jetts.co.th/wp-content/uploads/2024/04/240401_Promotion_Sales_Jetts_Apr24_1_Main.jpg',
        descript:
            'Get Fit With Garfield ราคาพิเศษเพียง 1,500 บาท/เดือน เมื่อสมัคร Jetts Fitness นาน 12 เดือน'),
    Promotion(
        imagePath:
            'https://www.jetts.co.th/wp-content/uploads/2024/04/PROMO-APRIL-2024_1-03_0-1024x1024.jpg',
        descript:
            'สมัครแพ็กเกจ Power Plate 36 Sessions รับฟรี 4 Sessions เฉพาะ ที่เกษรวิลเลจ หรือ The PARQ เท่านั้น'),
    Promotion(
        imagePath:
            'https://www.jetts.co.th/wp-content/uploads/2024/04/PROMO-APRIL-2024_1-04_0-1024x1024.jpg',
        descript:
            'สิทธิพิเศษ เฉพาะสมาชิกเจ็ทส์ ฟิตเรา ทดลอง Functional Training 1:1 ฟรี 1 ครั้ง'),
    Promotion(
        imagePath:
            'https://www.jetts.co.th/wp-content/uploads/2024/04/PROMO-APRIL-2024_30-02-1024x1024.jpg',
        descript:
            'โปรพิเศษ ฟิตหุ่นกับเทรนเนอร์ ตั้งแต่ 36 ครั้งขึ้นไป รับฟรีกระบอกน้ำลายการ์ฟีลด์ (Limited Edition)'),
  ];

  // List of News
  List<News> newsData = [
    News(
        imagePath:
            'https://www.jetts.co.th/wp-content/uploads/2024/04/240331_Reveal_JettsxGF-1024x1024.jpg'),
    News(
        imagePath:
            'https://www.jetts.co.th/wp-content/uploads/2024/04/001-min-1.jpg'),
    News(
        imagePath:
            'https://www.jetts.co.th/wp-content/uploads/2024/04/IMG_7021-min.jpg')
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(homeTitle,
                    style: const TextStyle(fontWeight: FontWeight.bold)),
                const SizedBox(width: 10),
                IconWithText(
                  text: 'Red',
                  svg: SvgPicture.asset(
                    AssetSVG.parallelogram,
                    colorFilter: const ColorFilter.mode(
                        AppColors.primaryColor, BlendMode.srcIn),
                    height: 22,
                    width: 22,
                  ),
                )
              ],
            ),
            Row(
              children: [
                SvgPicture.asset(
                  AssetSVG.location,
                ),
                Text(
                  locationTitle,
                  style: const TextStyle(
                      fontSize: 14, color: AppColors.textSecondary),
                ),
              ],
            ),
          ],
        ),
        actions: [
          GestureDetector(
            onTap: () {
              debugPrint("Tap Notify");
            },
            child: Container(
              margin: const EdgeInsets.only(right: 15),
              width: 45,
              height: 45,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: Color(0xFFFAFAFA),
              ),
              child: SvgPicture.asset(
                AssetSVG.notification,
                height: 15,
                width: 15,
                fit: BoxFit.scaleDown,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              debugPrint("Tap scan");
            },
            child: Container(
              margin: const EdgeInsets.only(right: 15),
              width: 45,
              height: 45,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: Color(0xFFFAFAFA),
              ),
              child: SvgPicture.asset(
                AssetSVG.scan,
                height: 15,
                width: 15,
                fit: BoxFit.scaleDown,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              debugPrint("Tap profile pic");
            },
            child: Container(
              width: 45,
              height: 45,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: Color.fromARGB(255, 153, 241, 241),
              ),
              child: const CircleAvatar(
                backgroundColor: Colors.grey,
                backgroundImage: NetworkImage(
                    'https://www.dmarge.com/wp-content/uploads/2021/01/dwayne-the-rock-.jpg'),
              ),
            ),
          ),
          const SizedBox(
            width: 20,
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(14, 25, 0, 0),
        child: ListView(
          children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    //โปรโมชัน
                    Column(
                      children: [
                        Container(
                          width: 75,
                          height: 75,
                          child: AspectRatio(
                            aspectRatio: 1,
                            child: Container(
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                              ),
                              child: const CircleAvatar(
                                backgroundColor: Colors.transparent,
                                backgroundImage: AssetImage(
                                    'assets/images/Rectangle_3468080.png'),
                              ),
                            ),
                          ),
                        ),
                        const Text("โปรโมชัน"),
                      ],
                    ),

                    //แพ็คเกจ
                    Column(
                      children: [
                        Container(
                          width: 75,
                          height: 75,
                          child: AspectRatio(
                            aspectRatio: 1,
                            child: Container(
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                              ),
                              child: const CircleAvatar(
                                backgroundColor: Colors.transparent,
                                backgroundImage: AssetImage(
                                    'assets/images/Rectangle_3468081.png'),
                              ),
                            ),
                          ),
                        ),
                        const Text("แพ็คเกจ"),
                      ],
                    ),

                    //คลาส
                    Column(
                      children: [
                        Container(
                          width: 75,
                          height: 75,
                          child: AspectRatio(
                            aspectRatio: 1,
                            child: Container(
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                              ),
                              child: const CircleAvatar(
                                backgroundColor: Colors.transparent,
                                backgroundImage: AssetImage(
                                    'assets/images/Rectangle_3468082.png'),
                              ),
                            ),
                          ),
                        ),
                        const Text("คลาส"),
                      ],
                    ),

                    //คลับ
                    Column(
                      children: [
                        Container(
                          width: 75,
                          height: 75,
                          child: AspectRatio(
                            aspectRatio: 1,
                            child: Container(
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                              ),
                              child: const CircleAvatar(
                                backgroundColor: Colors.transparent,
                                backgroundImage: AssetImage(
                                    'assets/images/Rectangle_3468083.png'),
                              ),
                            ),
                          ),
                        ),
                        const Text("คลับ"),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),

                // Promotion Title
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('โปรโมชั่น/สิทธิพิเศษ',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18)),
                    TextButton(
                        onPressed: () {},
                        child: const Text(
                          'ทั้งหมด',
                          style: TextStyle(
                              color: AppColors.primaryColor,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.underline,
                              decorationColor: AppColors.primaryColor),
                        ))
                  ],
                ),

                // Promotion Card
                Container(
                  height: 230,
                  child: ListView.builder(
                      padding: const EdgeInsets.fromLTRB(3, 5, 12, 12),
                      itemCount: promotionData.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        Promotion promotion = promotionData[index];
                        return PromotionCard(
                          promotion: promotion,
                        );
                      }),
                ),

                // News Title
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('ข่าวสาร/กิจกรรม',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18)),
                    TextButton(
                        onPressed: () {},
                        child: const Text(
                          'ทั้งหมด',
                          style: TextStyle(
                              color: AppColors.primaryColor,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.underline,
                              decorationColor: AppColors.primaryColor),
                        ))
                  ],
                ),

                // News Card
                SizedBox(
                  height: 160,
                  child: ListView.builder(
                      itemCount: newsData.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        News news = newsData[index];
                        return AspectRatio(
                          aspectRatio: 16 / 9,
                          child: Container(
                            margin: const EdgeInsets.only(right: 20),
                            width: 250,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(news.imagePath))),
                          ),
                        );
                      }),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
